The CUDA examples were taken from CUDA Tutorial https://cuda-tutorial.readthedocs.io/en/latest/.

## To compile a CUDA code

```
$ module load gcc/6.3.0 cuda/11.0.3
$ bsub -R "rusage[ngpus_excl_p=1]" -Is bash
$ nvcc vector_add.cu -o vector_add_cu
```

## To profile the CUDA executable

```
$ nvprof ./vector_add_cu
==37748== NVPROF is profiling process 37748, command: ./vector_add_cu
out[0] = 3.000000
PASSED
==37748== Profiling application: ./vector_add_cu
==37748== Profiling result:
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
 GPU activities:   91.11%  536.53ms         1  536.53ms  536.53ms  536.53ms  vector_add(float*, float*, float*, int)
                    5.76%  33.944ms         1  33.944ms  33.944ms  33.944ms  [CUDA memcpy DtoH]
                    3.13%  18.435ms         2  9.2175ms  9.1715ms  9.2635ms  [CUDA memcpy HtoD]

```
