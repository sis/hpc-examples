## Load module

```
$ env2lmod
$ module load gcc/6.3.0
```

## To compile the C code

```
$ gcc -fopenmp -o hello_omp hello_omp.c
```

## To compile the Fortran code

```
$ gfortran -fopenmp -o hello_omp hello_omp.f90
```

## To run

```
$ bsub -n 4 -Is bash
$ export OMP_NUM_THREADS=4
$ ./hello_omp
Hello world from thread            0
Hello world from thread            3
Hello world from thread            2
Hello world from thread            1
```

