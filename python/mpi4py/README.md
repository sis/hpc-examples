## Install mpi4py

```
$ env2lmod
$ module load gcc/6.3.0 openmpi/4.0.2 python/3.8.5
$ pip install --user mpi4py
```
## Submit a job

```
bsub -n 4 -R "span[ptile=1]" "mpirun -n 4 python hello_mpi4py.py"
```